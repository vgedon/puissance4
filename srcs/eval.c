/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 17:33:39 by vgedon            #+#    #+#             */
/*   Updated: 2014/03/09 18:37:19 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puissance4.h>
#include <libft.h>

int				eval(t_env *env)
{
	if (env->cpy[env->x][env->y] == env->player)
		return (env->col * env->row * 10 - env->nb_tour);
	else
		return (env->col * env->row * 10 * -1 + env->nb_tour);
}
