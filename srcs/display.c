/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 11:39:50 by vgedon            #+#    #+#             */
/*   Updated: 2014/03/08 15:45:20 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <puissance4.h>

void			display_separation_row(int col)
{
	int			i;

	i = 0;
	while (i < col)
	{
		ft_putstr(" ---");
		i++;
	}
	ft_putchar('\n');
}

void			display_pion(t_env *env, int x, int y)
{
	if (env->tab[x][y] == PLAYER1)
		ft_putstr(YELLOW);
	else
		ft_putstr(RED);
	ft_putchar(' ');
	ft_putstr(NORMAL);
}

void			display(t_env *env)
{
	int			x;
	int			y;

	y = env->row - 1;
	while (y >= 0)
	{
		display_separation_row(env->col);
		x = 0;
		while (x < env->col)
		{
			ft_putstr("| ");
			if (env->tab[x][y] > 0)
				display_pion(env, x, y);
			else
				ft_putchar(' ');
			ft_putstr(" ");
			x++;
		}
		ft_putstr("|\n");
		y--;
	}
	display_separation_row(env->col);
	display_column_number(env->col);
}

void			display_column_number(int col)
{
	int			i;

	i = 1;
	while (i <= col)
	{
		ft_printf(" %3d", i);
		i++;
	}
	ft_putchar('\n');
}
