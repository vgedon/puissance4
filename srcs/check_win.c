/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_win.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 11:09:26 by vgedon            #+#    #+#             */
/*   Updated: 2014/03/09 19:15:10 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puissance4.h>
#include <libft.h>

int				check_win(t_env *env)
{
	int			player;
	char		**tmp;

	tmp = (env->cpy != NULL ? env->cpy : env->tab);
	player = tmp[env->x][env->y];
	if (check_line(env, tmp, player) != OK
			|| check_col(env, tmp, player) != OK
			|| check_diag_up(env, tmp, player) != OK
			|| check_diag_down(env, tmp, player) != OK)
		return (PLAYER_WIN);
	return (OK);
}

int				check_line(t_env *env, char **tab, int player)
{
	int			x;
	int			y;
	int			count;

	x = env->x;
	y = env->y;
	count = 1;
	while ((x + 1) < env->col && tab[x + 1][y] == player)
	{
		x++;
		count++;
	}
	x = env->x;
	while ((x - 1) >= 0 && tab[x - 1][y] == player)
	{
		x--;
		count++;
	}
	if (count == 4)
		return (PLAYER_WIN);
	return (OK);
}

int				check_col(t_env *env, char **tab, int player)
{
	int			x;
	int			y;
	int			count;

	x = env->x;
	y = env->y;
	count = 1;
	while ((y + 1) < env->row && tab[x][y + 1] == player)
	{
		y++;
		count++;
	}
	y = env->y;
	while ((y - 1) >= 0 && tab[x][y - 1] == player)
	{
		y--;
		count++;
	}
	if (count == 4)
		return (PLAYER_WIN);
	return (OK);
}

int				check_diag_up(t_env *env, char **tab, int player)
{
	int			x;
	int			y;
	int			count;

	x = env->x;
	y = env->y;
	count = 1;
	while ((x + 1) < env->col && (y + 1) < env->row
			&& tab[x + 1][y + 1] == player)
	{
		x++;
		y++;
		count++;
	}
	x = env->x;
	y = env->y;
	while ((x - 1) >= 0 && (y - 1) >= 0 && tab[x - 1][y - 1] == player)
	{
		x--;
		y--;
		count++;
	}
	if (count == 4)
		return (PLAYER_WIN);
	return (OK);
}

int				check_diag_down(t_env *env, char **tab, int p)
{
	int			x;
	int			y;
	int			count;

	x = env->x;
	y = env->y;
	count = 1;
	while ((x + 1) < env->col && (y - 1) >= 0 && tab[x + 1][y - 1] == p)
	{
		x++;
		y--;
		count++;
	}
	x = env->x;
	y = env->y;
	while ((x - 1) >= 0 && (y + 1) < env->row && tab[x - 1][y + 1] == p)
	{
		x--;
		y++;
		count++;
	}
	if (count == 4)
		return (PLAYER_WIN);
	return (OK);
}
