/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 09:44:48 by vgedon            #+#    #+#             */
/*   Updated: 2014/03/09 20:40:19 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puissance4.h>
#include <libft.h>

int				is_full(t_env *env)
{
	int			i;

	i = 0;
	while (i < env->col)
	{
		if (env->tab[i][env->row - 1] == 0)
			return (OK);
		i++;
	}
	return (ERR_GRID_FULL);
}

int				is_win(t_env *env)
{
	if (check_win(env) != OK)
		return (PLAYER_WIN);
	if (is_full(env) != OK)
		return (ERR_GRID_FULL);
	return (OK);
}

int				play(t_env *env, int *player)
{
	int			err;

		*player = (*player == PLAYER1 ? PLAYER2 : PLAYER1);
		if (*player == PLAYER1)
			ft_putendl("\n\nplayer 1 move");
		else
			ft_putendl("\n\nplayer 2 move");
		if (env->player == *player)
		{
			if ((err = get_ia(env)) != OK)
				return (err);
		}
		else
		{
			if ((err = get_mouv(env, *player)) != OK)
				return (err);
		}
		display(env);
		env->nb_tour++;
		return (is_win(env));
}

int				main(int ac, char **av)
{
	int			err;
	int			player;
	t_env		*env;

	if (ac != 3 || (ft_error_number(av[1]) < 7) || (ft_error_number(av[2]) < 6))
		return (ft_error_init(ERR_ARG));
	if ((env = ft_init_env(env, av)) == NULL)
		return (ft_error_init(ERR_ENV));
	display(env);
	ft_putendl("vous pouvez quittez le jeu en tapant \"quit\"");
	player = PLAYER2;
	while (42)
		if ((err = play(env, &player)) != OK)
			return (ft_error_game(player, err));
	return (0);
}
