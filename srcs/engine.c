/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   engine.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvermand <mvermand@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/09 14:18:33 by mvermand          #+#    #+#             */
/*   Updated: 2014/03/09 20:38:34 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puissance4.h>
#include <libft.h>

char		**ft_cpy_tab(t_env *env, int i)
{
	int		j;

	if ((env->cpy = (char**)malloc(sizeof(char*) * env->row + 1)) == NULL)
		return (NULL);
	env->cpy[env->row] = '\0';
	while (i < env->col)
	{
		if ((env->cpy[i] = (char*)malloc(sizeof(char) * env->col + 1)) == NULL)
			return (NULL);
		env->cpy[i][env->col] = '\0';
		i++;
	}
	i = 0;
	while (i < env->col)
	{
		j = 0;
		while (j < env->row)
		{
			env->cpy[i][j] = env->tab[i][j];
			j++;
		}
		i++;
	}
	return (env->cpy);
}

int			ft_max(t_env *env, int prof)
{
	env->tmpx = env->x;
	env->tmpy = env->y;
	env->max = (env->row * env->col * 10) * -1;
	if (check_win(env) == PLAYER_WIN)
		return (env->max);
	if (prof == 0)
		return (eval(env));
	env->x = 0;
	while (env->x < env->col)
	{
		if ((env->y = ft_strlen(env->cpy[env->x])) == env->row)
			break ;
		env->cpy[env->x][env->y] = (env->player == PLAYER1 ? PLAYER2 : PLAYER1);
		env->tmp = ft_min(env, prof - 1);
		if (env->tmp < env->max)
			env->max = env->tmp;
		env->cpy[env->x][env->y] = 0;
		if ((env->x + 1) == env->col)
			break ;
		env->x++;
	}
	env->x = env->tmpx;
	env->y = env->tmpy;
	return (env->max);
}

int			ft_min(t_env *env, int prof)
{
	env->tmpx = env->x;
	env->tmpy = env->y;
	env->min = env->row * env->col * 10;
	if (check_win(env) == PLAYER_WIN)
		return (env->min);
	if (prof == 0)
		return (eval(env));
	env->x = 0;
	while (env->x < env->col)
	{
		if ((env->y = ft_strlen(env->cpy[env->x])) == env->row)
			break ;
		env->cpy[env->x][env->y] = env->player;
		env->tmp = ft_max(env, prof - 1);
		if (env->tmp > env->min)
			env->min = env->tmp;
		env->cpy[env->x][env->y] = 0;
		if ((env->x + 1) == env->col)
			break ;
		env->x++;
	}
	env->x = env->tmpx;
	env->y = env->tmpy;
	return (env->min);
}

int			ft_set_pos(t_env *env)
{
	int		tmp;
	int		max;

	max = (env->row * env->col * 10) * -1;
	env->y = ft_strlen(env->cpy[env->x]);
	env->cpy[env->x][env->y] = env->player;
	tmp = ft_min(env, env->prof - 1);
	if (tmp > max)
		max = tmp;
	env->cpy[env->x][env->y] = 0;
	return (max);
}

int			get_ia(t_env *env)
{
	int		max;
	int		tmp;

	env->x = 0;
	max = (env->row * env->col * 10) * -1;
	env->cpy = ft_cpy_tab(env, env->x);
	while (env->x < env->col)
	{
		if (ft_strlen(env->cpy[env->x]) < env->row)
		{
			tmp = ft_set_pos(env);
			if (tmp > max)
			{
				max = tmp;
				env->savex = env->x;
			}
		}
		env->x++;
	}
	env->x = env->savex;
	env->y = ft_strlen(env->tab[env->savex]);
	env->tab[env->x][env->y] = env->player;
	free_ctab(env->cpy);
	env->cpy = NULL;
	return (OK);
}
