/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 10:04:17 by vgedon            #+#    #+#             */
/*   Updated: 2014/03/09 16:00:10 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <puissance4.h>
#include <libft.h>

int				ft_error_number(char *str)
{
	int			nb;

	nb = ft_atoi(str);
	if (nb <= 0)
		return (-1);
	return (nb);
}

int				ft_error_init(int erreur)
{
	if (erreur == ERR_ARG)
		ft_putendl("Usage: ./puissance4 nb_colone(min 7) nb_ligne(min 6)");
	else if (erreur == ERR_ENV)
		ft_putendl("Error initialising env");
	return (1);
}

int				ft_error_game(int player, int err)
{
	if (err == ERR_READ)
		ft_putendl("Error reading input.");
	else if (err == QUIT)
		ft_putendl("Good Bye. See you soon.");
	else if (err == PLAYER_WIN)
		ft_printf("Player %d win!\n", player);
	else if (err == ERR_GRID_FULL)
		ft_putendl("No winner. the game is a draw.");
	return (1);
}
