/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 10:56:21 by vgedon            #+#    #+#             */
/*   Updated: 2014/03/09 20:27:53 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <puissance4.h>
#include <time.h>

int				choose_first_player(void)
{
	int			res;

	srand(time(NULL));
	res = (rand() % 2 ? PLAYER1 : PLAYER2);
	ft_printf("Computer is player %d\n", res);
	return (res);
}

t_env			*ft_init_env(t_env *env, char **av)
{
	int			i;
	int			size;

	i = 0;
	if ((env = (t_env*)ft_memalloc(sizeof(t_env))) == NULL)
		return (NULL);
	env->col = ft_atoi(av[1]);
	env->row = ft_atoi(av[2]);
	env->player = choose_first_player();
	env->prof = 4;
	env->nb_tour = 0;
	env->tab = (char**)ft_memalloc(sizeof(char*) * (env->col + 1));
	if (env->tab == NULL)
		return (NULL);
	while (i < env->col)
	{
		size = sizeof(char) * (env->row + 1);
		if ((env->tab[i] = (char*)ft_memalloc(size)) == NULL)
			return (NULL);
		ft_bzero(env->tab[i], sizeof(char) * env->row);
		i++;
	}
	return (env);
}

int				get_new_mouv(t_env *env, int player, int erreur)
{
	if (erreur == ERR_COL_INV)
	{
		ft_putendl("Numero de colonne invalide.");
		return (get_mouv(env, player));
	}
	else if (erreur == ERR_COL_FULL)
	{
		ft_putendl("Colonne pleine. Choisissez une autre colonne.");
		return (get_mouv(env, player));
	}
	return (get_mouv(env, player));
}

int				get_mouv(t_env *env, int player)
{
	int			i;
	int			col;
	char		*line;

	ft_putstr("Choisissez une colonne: ");
	if (get_next_line(STD_IN, &line) <= 0)
		return (ERR_READ);
	if (ft_strequ(line, "quit"))
		return (QUIT);
	col = ft_atoi(line);
	free_string(line);
	if (col <= 0 || col > env->col)
		return (get_new_mouv(env, player, ERR_COL_INV));
	i = 0;
	while (env->tab[col - 1][i] && i < env->row)
		i++;
	if (i == env->row)
		return (get_new_mouv(env, player, ERR_COL_FULL));
	env->tab[col - 1][i] = player;
	env->x = col - 1;
	env->y = i;
	return (OK);
}
