# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/19 11:30:52 by vgedon            #+#    #+#              #
#    Updated: 2014/03/09 17:38:08 by vgedon           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = puissance4

SRCDIR =  srcs/
SRC = 	main.c\
		error.c\
		env.c\
		display.c\
		check_win.c\
		engine.c\
		eval.c

OBJ = $(SRC:.c=.o)

INCLUDEDIR = ./includes

LIB = -L ./libft -lft

CC = gcc
CCFLAG = -Wall -Werror -Wextra -g

GREEN = \033[32m
NORMAL = \033[0m

all: $(NAME)
	@echo -n

$(NAME) : $(OBJ)
	@printf "%-30s" "Compiling library"
	@make -C libft/
	@printf '[$(GREEN) OK $(NORMAL)]\n'
	@printf "%-30s" "Compilimg $@"
	@$(CC) $(CCFLAG) -o $(NAME) $(OBJ) $(LIB)
	@printf '[ $(GREEN)OK$(NORMAL) ]\n'

%.o: $(addprefix $(SRCDIR), %.c)
	@printf "%-30s" "Compiling $@"
	@$(CC) $(CCFLAG) -c $^ -I $(INCLUDEDIR)
	@printf "[$(GREEN) OK $(NORMAL)]\n"

clean:
	@printf "%-30s" "Cleaning"
	@make -C libft/ clean
	@rm -rf $(OBJ)
	@printf "[$(GREEN) OK $(NORMAL)]\n"

fclean: clean
	@printf "%-30s" "Removing $(NAME)"
	@make -C libft/ fclean
	@rm -rf $(NAME)
	@printf "[ $(GREEN)OK$(NORMAL) ]\n"

re: fclean all

.PHONY: all clean fclean re
