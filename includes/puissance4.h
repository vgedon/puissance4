/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   puissance4.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/08 09:49:18 by vgedon            #+#    #+#             */
/*   Updated: 2014/03/09 20:29:34 by mvermand         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUISSANCE4_H
# define PUISSANCE4_H

/*
**	Definition
*/

typedef struct		s_env
{
	int				tmpx;
	int				min;
	int				tmpy;
	int				max;
	int				tmp;
	int				col;
	int				row;
	int				x;
	int				y;
	int				savex;
	int				savey;
	int				player;
	int				prof;
	int				nb_tour;
	char			**tab;
	char			**cpy;
}					t_env;

enum				e_player
{
	PLAYER1 = 1, PLAYER2 = 2
};

enum				e_err
{
	ERR_READ = -1, ERR_COL_INV, ERR_COL_FULL, ERR_ARG, ERR_ENV, ERR_GAME, QUIT,
	OK, PLAYER_WIN, ERR_GRID_FULL
};

# define RED "\033[41m"
# define YELLOW "\033[43m"
# define NORMAL "\033[0m"

/*
**	Description of sources files
**		file main.c
*/

int				is_full(t_env *env);
int				is_win(t_env *env);
int				play(t_env *env, int *player);
int				main(int ac, char **av);

/*
**		file error.c
*/

int				ft_error_init(int erreur);
int				ft_error_number(char *str);
int				ft_error_game(int player, int err);

/*
**		file env.c
*/

int				choose_first_player(void);
t_env			*ft_init_env(t_env *env, char **av);
int				get_new_mouv(t_env *env, int player, int erreur);
int				get_mouv(t_env *env, int player);

/*
**		file display.c
*/

void			display_separation_row(int col);
void			display_pion(t_env *env, int x, int y);
void			display(t_env *env);
void			display_column_number(int col);

/*
**		file check_win.c
*/

int				check_win(t_env *env);
int				check_line(t_env *env, char **tab, int player);
int				check_col(t_env *env, char **tab, int player);
int				check_diag_up(t_env *env, char **tab, int player);
int				check_diag_down(t_env *env, char **tab, int p);

/*
**		file engine.c
*/

char			**ft_cpy_tab(t_env *env, int i);
int				ft_max(t_env *env, int prof);
int				ft_min(t_env *env, int prof);
int				ft_set_pos(t_env *env);
int				get_ia(t_env *env);

/*
**		file eval.c
*/

int				eval(t_env *env);

#endif /* ! PUISSANCE4_H */
