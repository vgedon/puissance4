/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 09:54:35 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:23:11 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strdup(const char *src)
{
	char	*str;

	str = (char *)malloc(sizeof(src) * ft_strlen(src) + 1);
	if (str)
		ft_strcpy(str, src);
	return (str);
}

