/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 16:15:26 by vgedon            #+#    #+#             */
/*   Updated: 2013/12/02 14:55:20 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strchr(const char *str, int c)
{
	size_t		i;

	i = 0;
	if (str)
		while (str[i])
		{
			if (str[i] == (char) c)
				return ((char *) &str[i]);
			i++;
		}
	if (!c)
		return ((char *) &str[i]);
	return (NULL);
}

