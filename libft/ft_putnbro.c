/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbro.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/13 14:41:20 by vgedon            #+#    #+#             */
/*   Updated: 2014/02/11 14:16:58 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_getnbro(unsigned int n, char sharp)
{
	int		i;
	char	*res;

	res = ft_strnew(20);
	i = 0;
	if (!n)
		res[0] = '0';
	while (n > 0)
	{
		res[i] = (n % 8) + '0';
		n = n / 8;
		i++;
	}
	if (sharp && n)
		res[i] = '0';
	i++;
	res[i] = 0;
	ft_strrev(res);
	return (res);
}

