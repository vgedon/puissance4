/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/11 10:14:35 by vgedon            #+#    #+#             */
/*   Updated: 2013/12/11 10:43:00 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int				ft_getnbr(char *s)
{
	int			i;
	int			nbr;
	int			neg;
	int			count;

	i = 0;
	count = 0;
	nbr = 0;
	neg = 1;
	while (!ft_isdigit(s[i]))
	{
		if (s[i] == '-')
			count++;
		if (s[i] != '-' || s[i] != '+')
			count = 0;
		i++;
	}
	while (ft_isdigit(s[i]))
	{
		nbr = nbr * 10 + (s[i] - '0');
		i++;
	}
	if (count % 2)
		neg = -1;
	return (neg * nbr);
}
