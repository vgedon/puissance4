/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 15:07:02 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/13 14:43:13 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int		ft_is_ws(int c)
{
	if ((c >= 9 && c <= 13) || c == 32)
		return (1);
	return (0);
}

int				ft_atoi(char *str)
{
	int		nb;
	int		neg;
	int		strt;

	nb = 0;
	strt = 0;
	while (ft_is_ws(str[strt]))
		strt++;
	neg = 1;
	if (str[strt] == '-')
		neg = -1;
	if (ft_isdigit(str[strt + 1]) && (str[strt] == '-' || str[strt] == '+'))
		strt++;
	while (ft_isdigit(str[strt]))
	{
		nb = nb  * 10 + (str[strt] - 48);
		strt++;
	}
	return (nb * neg);
}

