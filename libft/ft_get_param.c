/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_param.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/20 17:01:56 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:18:42 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_flag			*ft_fill_flag(char *str, t_flag *flag)
{
	while (ft_isflag(*str))
	{
		flag->plus = (*str == '+' ? 1 : 0);
		flag->space = (*str == ' ' ? 1 : 0);
		flag->neg = (*str == '-' ? 1 : 0);
		flag->sharp = (*str == '#' ? 1 : 0);
		flag->zero = (*str == '0' ? 1 : 0);
		str++;
	}
	flag->neg = (flag->zero ? 1 : 0);
	if (ft_isdigit(*str))
	{
		flag->width = ft_getnbr(str);
		while (ft_isdigit(*str))
			str++;
	}
	if ((flag->point = (*str == '.' ? 1 : 0)))
	{
		flag->precision = ft_getnbr(str);
		while (ft_isdigit(*str) || *str == '.')
			str++;
	}
	if (ft_istype(*str))
		flag->type = *str;
	return (flag);
}

t_flag			*ft_init_flag(t_flag *flag, char **str, int *len)
{
	if (flag == NULL)
		flag = (t_flag *)malloc(sizeof(t_flag));
	flag->str = str;
	flag->type = 0;
	flag->width = 0;
	flag->precision = 0;
	flag->plus = 0;
	flag->space = 0;
	flag->neg = 0;
	flag->sharp = 0;
	flag->zero = 0;
	flag->len = len;
	return (flag);
}

int				ft_isflag(char str)
{
	if (str == '+' || str == ' ' || str == '-' || str == '#' || str == '0')
		return (1);
	return (0);
}

int				ft_istype(char str)
{
	int			i;
	char		tab[ARG_S] = ARG;

	i = 0;
	while (tab[i] != str && i < ARG_S)
		i++;
	if (i < ARG_S)
		return (1);
	return (0);
}

void			ft_print_width(int width, char c)
{
	while (width)
	{
		write(1, &c, 1);
		width--;
	}
}
