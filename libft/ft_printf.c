/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/17 17:11:50 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:20:58 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char			*ft_get_flag(char **str, int *len, t_flag *flag, va_list ap)
{
	int			i;
	char		*tmp;

	i = 0;
	tmp = *str;
	flag = ft_init_flag(flag, str, len);
	while (tmp[i] && !ft_isalpha(tmp[i]) && tmp[i] != '%')
		i++;
	if (!tmp[i] || !ft_istype(tmp[i]) || tmp[i] == '%')
	{
		write(1, *str, 1);
		*len = *len + 1;
		return (*str);
	}
	else
	{
		flag = ft_fill_flag(tmp, flag);
		*len = *len + ft_printf_arg(flag, ap);
	}
	return (*str + i);
}

int				ft_printf_arg(t_flag *flag, va_list ap)
{
	int			i;
	int			(*pt_ft[ARG_S])(t_flag *, va_list) = ARG_P ARG_P2 ARG_P3;
	char		test[ARG_S] = ARG;

	i = 0;
	while (test[i] != flag->type && i < ARG_S)
		i++;
	return (pt_ft[i](flag, ap));
}

int				ft_printf(const char *str, ...)
{
	int			len;
	va_list		ap;
	t_flag		*flag;

	va_start(ap, str);
	len = 0;
	flag = NULL;
	while (*str != '\0')
	{
		if (*str == '%')
		{
			str++;
			str = ft_get_flag((char **)&str, &len, flag, ap);
		}
		else
		{
			write(1, str, 1);
			len++;
		}
		str++;
	}
	free(flag);
	va_end(ap);
	return (len);
}
