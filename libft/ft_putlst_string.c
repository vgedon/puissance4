/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putlst_string.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 14:54:42 by vgedon            #+#    #+#             */
/*   Updated: 2014/03/02 12:09:06 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void			ft_putlst_string(t_list **lst)
{
	t_list		*tmp;

	tmp = *lst;
	while (tmp != NULL)
	{
		if (tmp->content != NULL)
			ft_putendl((char *)tmp->content);
		else
			ft_putendl("(null)");
		tmp = tmp->next;
	}
}
