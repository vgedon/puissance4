/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoxmin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/20 09:58:31 by vgedon            #+#    #+#             */
/*   Updated: 2014/02/11 14:16:58 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void				ft_itoxmin(unsigned long nb, char *hex, char sharp)
{
	int				i;
	int				j;
	char			tab[] = "0123456789abcdef";

	j = 0;
	while (nb > 15)
	{
		i = nb % 16;
		hex[j] = tab[i];
		nb = nb / 16;
		j++;
	}
	hex[j] = tab[nb];
	if (sharp)
	{
		hex[j + 1] = 'x';
		hex[j + 2] = '0';
		j = j + 2;
	}
	hex[j + 1] = '\0';
	hex = ft_strrev(hex);
}

