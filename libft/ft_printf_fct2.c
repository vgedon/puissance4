/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_fct2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 16:38:04 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:21:23 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int					ft_printf_p(t_flag *flag, va_list ap)
{
	unsigned long	p;
	void			*ptr;
	int				ret;
	int				width;
	char			*res;

	res = ft_strnew(25);
	ptr = va_arg(ap, void *);
	p = (unsigned long)ptr;
	ft_itoxmin(p, res, 1);
	ret = ft_strlen(res);
	width = flag->width - ret;
	ret = (width > 0 ? ret + width + flag->sharp : ret + flag->sharp);
	if (!flag->neg && width > 0)
		ft_print_width(width, ' ');
	ft_putstr(res);
	if (flag->neg && width > 0)
		ft_print_width(width, ' ');
	free(res);
	return (ret);
}

int					ft_printf_s(t_flag *flag, va_list ap)
{
	int				i;
	int				ret;
	int				precision;
	int				width;
	char			*str;

	i = 0;
	str = va_arg(ap, char *);
	ret = (str ? ft_strlen(str) : 6);
	width = flag->width - ret;
	ret = (width > 0 ? ret + width : ret);
	if (!flag->neg && width > 0)
		ft_print_width(width, ' ');
	if (!str)
		str = "(null)";
	precision = (flag->point ? flag->precision : ft_strlen(str));
	while (i < precision)
	{
		write(1, &str[i], 1);
		i++;
	}
	if (flag->neg && width > 0)
		ft_print_width(width, ' ');
	return (ret);
}

int					ft_printf_c(t_flag *flag, va_list ap)
{
	int				c;
	int				ret;
	int				width;

	c = va_arg(ap, int);
	ret = 1;
	width = flag->width - ret;
	ret = (width > 0 ? ret + width : ret);
	if (!flag->neg && width > 0)
		ft_print_width(width, ' ');
	write(1, &c, 1);
	if (flag->neg && width > 0)
		ft_print_width(width, ' ');
	return (ret);
}

int					ft_printf_d(t_flag *flag, va_list ap)
{
	int				ret;
	int				d;
	int				width;
	char			c;
	char			*res;

	d = va_arg(ap, int);
	res = ft_getnbrd(d, flag->plus);
	ret = ft_strlen(res);
	c = (flag->zero ? '0' : ' ');
	c = (flag->point ? '0' : c);
	width = flag->width - ret - flag->sharp;
	width = (flag->point ? flag->precision - flag->width : width);
	if (flag->space && width <= 0 && d >= 0)
		width = 1;
	ret = (width > 0 ? ret + width + flag->sharp : ret + flag->sharp);
	if (!flag->neg && width > 0)
		ft_print_width(width, c);
	ft_putstr(res);
	if (flag->neg && width > 0)
		ft_print_width(width, ' ');
	free(res);
	return (ret);
}

