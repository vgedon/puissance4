/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 09:31:42 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:25:51 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static size_t	ft_strsplit_count(char const *s, char c)
{
	size_t		i;
	size_t		wrd;
	size_t		wrd_cnt;

	i = 0;
	wrd = 0;
	wrd_cnt = 0;
	while (s[i])
	{
		if (s[i] == c && wrd)
		{
			wrd = 0;
			wrd_cnt++;
		}
		else if (s[i] != c && !wrd)
			wrd = 1;
		i++;
	}
	if (wrd)
		wrd_cnt++;
	return (wrd_cnt);
}

static void		ft_split_fill(char **dest, char const *s, char c)
{
	size_t		i;
	size_t		wrd;
	size_t		wrd_strt;

	i = 0;
	wrd = 0;
	while (s[i])
	{
		if (s[i] == c)
			while (s[i] == c)
				i++;
		else
		{
			wrd_strt = i;
			while (s[i] && s[i] != c)
				i++;
			dest[wrd] = ft_strsub(s, wrd_strt, i - wrd_strt);
			wrd++;
		}
	dest[wrd] = '\0';
	}
}

char			**ft_strsplit(char const *s, char c)
{
	size_t		size;
	char		**tmp;

	tmp = NULL;
	if (s)
	{
		size = ft_strsplit_count(s, c);
		tmp = (char **) malloc(sizeof(tmp) * (size + 1));
		ft_split_fill(tmp, s, c);
	}
	return (tmp);
}

