/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/19 16:15:26 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:25:41 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strrchr(const char *str, int c)
{
	size_t			i;
	char			*result;

	i = 0;
	result = NULL;
	if (str)
		while (str[i])
		{
			if (str[i] == (char) c)
				result = (char *) &str[i];
			i++;
		}
	if (!c)
		return ((char *) &str[i]);
	return (result);
}

