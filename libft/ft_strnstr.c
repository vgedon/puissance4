/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 10:08:00 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:25:27 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strnstr(const char *str, const char *to_find, size_t n)
{
	size_t		i;
	size_t		j;
	size_t		res;

	i = 0;
	j = 0;
	if (!to_find || !ft_strlen(to_find))
		return ((char *) str);
	while (str[i] && i < n)
	{
		j = 0;
		res = i;
		while (to_find[j] == str[i] && str[i] && to_find[j] && i < n)
		{
			i++;
			j++;
		}
		if (!to_find[j])
			return ((char *) &str[res]);
		i = res + 1;
	}
	return (0);
}

