/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 10:07:06 by vgedon            #+#    #+#             */
/*   Updated: 2013/12/22 17:02:34 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static size_t	ft_chr_pwr(long int n)
{
	size_t		i;

	i = 0;
	while (n >= 10)
	{
		i++;
		n = n / 10;
	}
	return (i);
}

static int		ft_ten_pwr(int pwr)
{
	if (!pwr)
		return (1);
	else
		return (10 * ft_ten_pwr(pwr - 1));
}

char			*ft_itoa(int n)
{
	int			pwr;
	size_t		i;
	char		*result;
	long int	tmp;

	i = 0;
	tmp = (long int)n;
	if (n < 0)
	{
		tmp = tmp * -1;
		i++;
	}
	pwr = ft_chr_pwr(tmp);
	result = ft_strnew(pwr + i + 2);
	if (!result)
		return (NULL);
	result[0] = '-';
	while (pwr >= 0)
	{
		result[i] = ((tmp / ft_ten_pwr(pwr)) % 10) + 48;
		pwr--;
		i++;
	}
	return (result);
}

