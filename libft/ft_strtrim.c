/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 09:17:25 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/13 14:51:47 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static int			ft_is_space(int c)
{
	if (c == ' ' || c == '\t' || c == '\n')
		return (1);
	return (0);
}

static size_t		ft_strtrim_end(char const *s, size_t end)
{
	while (ft_is_space(s[end]) && end)
		end--;
	return (end);
}

static size_t		ft_strtrim_strt(char const *s, size_t strt)
{
	while (ft_is_space(s[strt]) && s[strt])
			strt++;
	return (strt);
}

char				*ft_strtrim(char const *s)
{
	size_t			strt;
	size_t			end;
	char			*tmp;

	if (!s)
		return (NULL);
	end = (size_t) ft_strlen(s);
	tmp = NULL;
	end = ft_strtrim_end(s, end - 1);
	strt = ft_strtrim_strt(s, 0);
	if (strt < end)
	{
		tmp = ft_strsub(s, strt, (end - strt + 1));
	}
	else
		tmp = ft_strnew(0);
	return (tmp);
}

