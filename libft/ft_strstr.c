/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 10:08:00 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:26:03 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char	*ft_strstr(const char *str, const char *to_find)
{
	size_t		i;
	size_t		j;
	size_t		res;

	if (!to_find || ft_strlen(to_find) == 0)
		return ((char *) str);
	i = 0;
	j = 0;
	while (str[i])
	{
		res = i;
		j = 0;
		while (to_find[j] == str[i] && to_find[j] && str[i])
		{
			i++;
			j++;
		}
		if (!to_find[j])
			return ((char *) &str[res]);
		i = res + 1;
	}
	return (NULL);
}

