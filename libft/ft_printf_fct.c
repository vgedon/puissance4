/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_fct.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/18 17:38:24 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:21:11 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int					ft_printf_xmaj(t_flag *flag, va_list ap)
{
	unsigned int	x;
	int				width;
	int				ret;
	char			c;
	char			*res;

	res = ft_strnew(20);
	x = va_arg(ap, unsigned int);
	ft_itoxmaj(x, res, flag->sharp);
	ret = ft_strlen(res);
	c = (flag->zero ? '0' : ' ');
	c = (flag->point ? '0' : c);
	width = flag->width - ret;
	width = (flag->point ? flag->precision - ret : width);
	ret = (width > 0 ? ret + width + flag->sharp : ret + flag->sharp);
	if (!flag->neg && width > 0)
		ft_print_width(width, c);
	ft_putstr(res);
	if (flag->neg && width > 0)
		ft_print_width(width, ' ');
	free(res);
	return (ret);
}

int					ft_printf_xmin(t_flag *flag, va_list ap)
{
	unsigned long	x;
	int				width;
	int				ret;
	char			c;
	char			*res;

	res = ft_strnew(20);
	x = va_arg(ap, unsigned long);
	ft_itoxmin(x, res, flag->sharp);
	ret = ft_strlen(res);
	c = (flag->zero ? '0' : ' ');
	c = (flag->point ? '0' : c);
	width = flag->width - ret;
	width = (flag->point ? flag->precision - ret : width);
	ret = (width > 0 ? ret + width + flag->sharp : ret + flag->sharp);
	if (!flag->neg && width > 0)
		ft_print_width(width, c);
	ft_putstr(res);
	if (flag->neg && width > 0)
		ft_print_width(width, ' ');
	free(res);
	return (ret);
}

int					ft_printf_o(t_flag *flag, va_list ap)
{
	unsigned int	o;
	int				width;
	int				ret;
	char			c;
	char			*res;

	o = va_arg(ap, unsigned int);
	res = ft_getnbro(o, flag->sharp);
	ret = ft_strlen(res);
	c = (flag->zero ? '0' : ' ');
	c = (flag->point ? '0' : c);
	width = flag->width - ret - flag->sharp;
	width = (flag->point ? flag->precision - flag->width : width);
	ret = (width > 0 ? ret + width + flag->sharp : ret + flag->sharp);
	if (!flag->neg && width > 0)
		ft_print_width(width, c);
	ft_putstr(res);
	if (flag->neg && width > 0)
		ft_print_width(width, ' ');
	free(res);
	return (ret);
}

int					ft_printf_u(t_flag *flag, va_list ap)
{
	unsigned int	u;
	int				width;
	int				ret;
	char			c;
	char			*res;

	u = va_arg(ap, unsigned int);
	res = ft_getnbru(u);
	ret = ft_strlen(res);
	width = flag->width - ret;
	c = (flag->zero ? '0' : ' ');
	ret = (width > 0 ? ret + width : ret);
	if (!flag->neg && width > 0)
		ft_print_width(width, c);
	ft_putstr(res);
	if (flag->neg && width > 0)
		ft_print_width(width, ' ');
	free(res);
	return (ret);
}

