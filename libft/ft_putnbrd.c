/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbrd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/22 17:07:55 by vgedon            #+#    #+#             */
/*   Updated: 2014/02/11 14:16:58 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char			*ft_getnbrd(int n, char plus)
{
	int			i;
	char		*res;
	long int	tmp;

	res = ft_strnew(20);
	i = 0;
	tmp = (n >= 0 ? (long int)n : (long int)n * -1);
	if (n == 0)
	{
		res[0] = '0';
		i++;
	}
	while (tmp > 0)
	{
		res[i] = (tmp % 10) + '0';
		tmp = tmp / 10;
		i++;
	}
	res[i] = (n < 0 ? '-' : 0);
	res[i] = (n >= 0 && plus ? '+' : res[i]);
	res[i + 1] = 0;
	ft_strrev(res);
	return (res);
}

