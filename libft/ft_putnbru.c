/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbru.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/13 14:41:43 by vgedon            #+#    #+#             */
/*   Updated: 2014/02/11 14:16:58 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_getnbru(unsigned int n)
{
	int		i;
	char	*res;

	res = ft_strnew(20);
	i = 0;
	if (!n)
		res[0] = '0';
	while (n > 0)
	{
		res[i] = (n % 10) + '0';
		n = n / 10;
		i++;
	}
	ft_strrev(res);
	return (res);
}

