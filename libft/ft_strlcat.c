/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 13:42:04 by vgedon            #+#    #+#             */
/*   Updated: 2013/12/02 18:10:05 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

size_t				ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t			dst_len;
	size_t			res;
	size_t			i;
	size_t			j;

	dst_len = ft_strlen(dst);
	res = dst_len + ft_strlen(src);
	i = 0;
	j = 0;
	if (size <= dst_len)
		res = res - (dst_len - size);
	else
	{
		size = size - (dst_len + 1);
		while (dst[i])
			i++;
		while (src[j] && j < size)
		{
			dst[i] = src[j];
			i++;
			j++;
		}
		dst[i] = '\0';
	}
	return (res);
}
