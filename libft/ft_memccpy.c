/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 11:26:16 by vgedon            #+#    #+#             */
/*   Updated: 2013/12/02 14:49:04 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void	*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	size_t					i;
	unsigned char			*dest;
	unsigned const char		*src;

	dest = (unsigned char *) s1;
	src = (unsigned const char *) s2;
	i = 0;
	while (i < n)
	{
		if ((*dest++ = *src++) == (unsigned char) c)
			return (dest);
		i++;
	}
	return (NULL);
}

