/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 16:37:47 by vgedon            #+#    #+#             */
/*   Updated: 2013/12/11 10:43:59 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_strmap(char const *s, char (*f)(char))
{
	int			i;
	char		*tmp;

	i = 0;
	tmp = ft_strnew(ft_strlen(s) + 1);
	if (s && f)
	{
		if (!tmp)
			return (NULL);
		while (s[i])
		{
			tmp [i] = f(s[i]);
			i++;
		}
	}
	else
		free(tmp);
	return (tmp);
}

