/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 12:21:43 by vgedon            #+#    #+#             */
/*   Updated: 2014/03/02 12:06:49 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

/*
** Includes and defines
*/
# include <unistd.h>
# include <string.h>
# include <stdlib.h>
# include <stdarg.h>

# define ARG_S 9
# define TYPE {'+', ' ', '-', '#', '0'}
# define TYPE_SIZE 5
# define ARG {'d', 'i', 'c', 's', 'u', 'o', 'x', 'X', 'p'}
# define ARG_P {ft_printf_d, ft_printf_d, ft_printf_c,
# define ARG_P2 ft_printf_s, ft_printf_u, ft_printf_o, ft_printf_xmin,
# define ARG_P3 ft_printf_xmaj, ft_printf_p}
# define BUFF_SIZE 8

typedef enum		e_std
{
	STD_IN = 0, STD_OUT = 1, STD_ERR = 2
}					t_std;

typedef struct		s_flag
{
	char			**str;
	char			type;
	int				width;
	char			point;
	int				precision;
	char			plus;
	char			space;
	char			neg;
	char			sharp;
	char			zero;
	int				*len;
}					t_flag;

typedef struct			s_list
{
	void				*content;
	size_t				content_size;
	struct s_list		*next;
}						t_list;

/*
**	Function prototypes
*/

int			ft_abs(int n);
int			ft_atoi(char *s);
void		ft_bzero(void *s, size_t n);
int			ft_isalnum(int c);
int			ft_isalpha(int c);
int			ft_isascii(int c);
int			ft_isdigit(int c);
int			ft_isprint(int c);
int			ft_iswhitespace(int c);
char		*ft_itoa(int n);
int			ft_getnbr(char *s);

void		ft_lstadd(t_list **alst, t_list *new);
void		ft_lstaddend(t_list **alst, t_list *new);
void		ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void		ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void		ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list		*ft_lstnew(void const *content, size_t content_size);
void		ft_putlst_string(t_list **lst);

void		*ft_memalloc(size_t size);
void		*ft_memccpy(void *s1, const void *s2, int c, size_t n);
void		*ft_memchr(const void *s, int c, size_t n);
int			ft_memcmp(const void *s1, const void *s2, size_t n);
void		*ft_memcpy(void *s1, const void *s2, size_t n);
void		ft_memdel(void **ap);
void		*ft_memjoin(const void *s1, const void *s2, size_t t1, size_t t2);
void		*ft_memmove(void *s1, const void *s2, size_t n);
void		*ft_memset(void *b, int c, size_t len);
void		free_ctab(char **str);
void		free_string(char *str);

void		ft_putaddress(void *ptr);
void		ft_putchar(char c);
void		ft_putchar_fd(char c, int fd);
void		ft_putendl(const char *s);
void		ft_putendl_fd(const char *s, int fd);
void		ft_putnbr(int n);
void		ft_putnbr_fd(int n, int fd);
void		ft_putstr(const char *s);
void		ft_putstr_fd(const char *s, int fd);

char		*ft_strcat(char *dest, const char *src);
char		*ft_strchr(const char *str, int c);
void		ft_strclr(char *s);
int			ft_strcmp(const char *s1, const char *s2);
char		*ft_strcpy(char *dest, const char *src);
void		ft_strdel(char **as);
char		*ft_strdup(const char *src);
int			ft_strequ(char const *s1, char const *s2);
void		ft_striter(char *s, void (*f)(char *));
void		ft_striteri(char *s, void (*f)(unsigned int, char *));
char		*ft_strjoin(char const *s1, char const *s2);
size_t		ft_strlcat(char *dest, const char *src, size_t size);
int			ft_strlen(const char *str);
char		*ft_strmap(char const *s, char (*f)(char));
char		*ft_strmapi(char const *s, char (*f)(unsigned int, char));
char		*ft_strncat(char *dest, const char *src, int nb);
int			ft_strncmp(const char *s1, const char *s2, size_t n);
char		*ft_strncpy(char *dest, const char *src, size_t n);
int			ft_strnequ(char const *s1, char const *s2, size_t n);
char		*ft_strnew(size_t size);
char		*ft_strnstr(const char *str, const char *to_find, size_t n);
char		*ft_strrchr(const char *str, int c);
char		**ft_strsplit(char const *s, char c);
char		*ft_strstr(const char *str, const char *to_find);
char		*ft_strsub(char const *s, unsigned int start, size_t len);
char		*ft_strtrim(char const *s);
int			ft_tolower(int c);
int			ft_toupper(int c);

int			get_next_line(int const fd, char **line);

t_flag		*ft_fill_flag(char *str, t_flag *flag);
t_flag		*ft_init_flag(t_flag *flag, char **str, int *len);
char		*ft_get_flag(char **str, int *len, t_flag *flag, va_list ap);
int			ft_printf_arg(t_flag *flag, va_list ap);
int			ft_printf(const char *str, ...);
int			ft_printf_d(t_flag *flag, va_list ap);
int			ft_printf_c(t_flag *flag, va_list ap);
int			ft_printf_o(t_flag *flag, va_list ap);
int			ft_printf_p(t_flag *flag, va_list ap);
int			ft_printf_s(t_flag *flag, va_list ap);
int			ft_printf_u(t_flag *flag, va_list ap);
int			ft_printf_xmaj(t_flag *flag, va_list ap);
int			ft_printf_xmin(t_flag *flag, va_list ap);
int			ft_isflag(char str);
int			ft_istype(char str);
void		ft_itoxmin(unsigned long nb, char *hex, char sharp);
void		ft_itoxmaj(unsigned int nb, char *hex, char sharp);
char		*ft_getnbru(unsigned int n);
int			ft_putnbrd(int n);
char		*ft_getnbro(unsigned int n, char sharp);
char		*ft_getnbrd(int n, char plus);
char		*ft_strrev(char *str);
void		ft_print_width(int width, char c);

#endif /* ! libft.h */

