/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 08:54:16 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:26:14 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char		*tmp;

	if (!s)
		return (NULL);
	tmp = ft_strnew(len + 1);
	if (!tmp)
		return (NULL);
	tmp = (char *) ft_memcpy(tmp, &s[start], len);
	return (tmp);
}

