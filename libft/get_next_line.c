/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/03 13:19:41 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/13 14:53:07 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static char				*ft_strchr_gnl(const char *buff, unsigned int *i)
{
	while (buff[*i] != '\0')
	{
		if (buff[*i] == '\n')
			return ((char *) &buff[*i + 1]);
		*i = *i + 1;
	}
	return (NULL);
}

static int				ft_get_line(char *buff, char **line, char **tmp)
{
	unsigned int		i;
	char				*end;

	i = 0;
	end = ft_strnew(BUFF_SIZE + 1);
	end = ft_strchr_gnl(buff, &i);
	if (end == NULL)
	{
		*line = ft_strjoin(*line, buff);
		*tmp[0] = '\0';
		return (0);
	}
	else
	{
		*line = ft_strjoin(*line, ft_strsub(buff, 0, i));
		*tmp = end;
		return (1);
	}
	return (0);
}

size_t					ft_read(int fd, char *buff)
{
	size_t				ret;

	ret = read(fd, buff, BUFF_SIZE);
	buff[ret] = '\0';
	return (ret);
}

int						get_next_line(int fd, char **line)
{
	static char			*buff = NULL;
	char				*tmp;
	int					stop;
	size_t				ret;

	if (fd < 0)
		return (-1);
	if (buff == NULL)
		buff = ft_strnew(BUFF_SIZE + 1);
	*line = ft_strnew(1);
	tmp = ft_strnew(BUFF_SIZE + 1);
	ret = 1;
	stop = 0;
	while (ret > 0 && stop == 0)
	{
		if (buff[0] == '\0')
			ret = ft_read(fd, buff);
		stop = ft_get_line(buff, line, &tmp);
		ft_strcpy(buff, tmp);
	}
	if (ret > 0)
		return (1);
	return (ret);
}

