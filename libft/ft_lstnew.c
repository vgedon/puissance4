/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/29 10:58:34 by vgedon            #+#    #+#             */
/*   Updated: 2013/12/13 15:49:18 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

t_list				*ft_lstnew(void const *content, size_t content_size)
{
	t_list			*new;
	void			*tmp;

	new = (t_list *) malloc(sizeof(*new));
	if (!new)
		return (NULL);
	if (content)
	{
		if ((tmp = ft_memalloc(content_size)) == NULL)
			return (NULL);
		new->content = ft_memcpy(tmp, content, content_size);
		new->content_size = content_size;
	}
	else
	{
		new->content = NULL;
		new->content_size = 0;
	}
	new->next = NULL;
	return (new);
}

