/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoxmaj.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/20 10:40:29 by vgedon            #+#    #+#             */
/*   Updated: 2014/02/11 14:16:58 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

void				ft_itoxmaj(unsigned int nb, char *hex, char sharp)
{
	int				i;
	int				j;
	char			tab[] = "0123456789ABCDEF";

	j = 0;
	while (nb > 15)
	{
		i = nb % 16;
		hex[j] = tab[i];
		nb = nb / 16;
		j++;
	}
	hex[j] = tab[nb];
	if (sharp)
	{
		j++;
		hex[j] = 'X';
		j++;
		hex[j] = '0';
	}
	hex[j + 1] = '\0';
	hex = ft_strrev(hex);
}
