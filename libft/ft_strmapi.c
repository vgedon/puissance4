/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 08:24:14 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:23:50 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	size_t		i;
	char		*tmp;

	i = 0;
	tmp = ft_strnew(ft_strlen(s) + 1);
	if (s && f)
	{
		if (!tmp)
			return (NULL);
		while (s[i])
		{
			tmp[i] = f(i, s[i]);
			i++;
		}
	}
	else
		free(tmp);
	return (tmp);
}

