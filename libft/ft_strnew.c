/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vgedon <vgedon@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 15:48:57 by vgedon            #+#    #+#             */
/*   Updated: 2014/01/06 10:25:17 by vgedon           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

char		*ft_strnew(size_t size)
{
	char	*new;

	new = (char *) malloc(sizeof(char) * size);
	if (!new)
		return (NULL);
	ft_bzero(new, size);
	return (new);
}

